# issues

Document issues here.  Each file should look like this:

Sample filename:
`2023-0913-01-descriptive_name.md`

```
[TOC]
# descriptive name : [waiting | assigned | complete]

Describe the issue in full detail here.

## 2023-0915 [description]

Put updates to what was learned or resolved here.  Keep the most recent on
top.

## 2023-0914

Each bit of information should be logged under a separate date.
```

