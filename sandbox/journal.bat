@echo off
setlocal
echo.

rem ------------------------------------------------------------
rem Create a Markdown file under the %APPDATA%\%USERNAME%\journal directory
rem with the date code name  of the form 'yyyyMM' (for example, '201609.md').
rem Then append a horizontal line with a date and time below it and then send
rem it to an EDITOR so that the user can freely enter information.
rem
rem With no arguments, this script creates/adds to a journal file with the
rem current date code.
rem
rem Several keywords arguments are available, use 'journal help' for details.
rem
rem Any other argument is taken as the base name of a custom journal where
rem no date code is appended.
rem ------------------------------------------------------------

pushd "%APPDATA%"
if ERRORLEVEL 1 call :SW "APPDATA is not defined as a valid directory" & exit/b 1
if not exist %USERNAME%\journal mkdir %USERNAME%\journal
if ERRORLEVEL 1 call :SW "Unable to use the APPDATA directory" & exit/b 1
cd %USERNAME%\journal
set JDIR=%CD%
popd

rem Remove leading dashes for those who are used to unix-like options
set param=%*
if "%param:~0,2%" == "--" (
      set param=%param:~2%
      )
if "%param:~0,1%" == "-" (
      set param=%param:~1%
      )

rem Remove spaces from the parameter
if not "%param%" == "" set param=%param: =-%


rem Set the EDITOR
if not exist "%EDITOR%" set EDITOR=%ProgramFiles%\Windows NT\Accessories\wordpad.exe
if not exist "%EDITOR%" goto :SW "EDITOR is not defined properly"



rem Since gvim has a nasty feature of moving the default
rem directory when the volume letter is used, remove it
rem
rem FIXME: does not work if HOME is a UNC path to a server.
rem if /i "%CD:~0,3%" == "%JDIR:~0,3%" set JDIR=%JDIR:~2%


if /i "%param%" == "help" (
      echo journal v3.0.0
      echo.
      echo usage:
      echo    journal
      echo       automatically create/update this month's journal
      echo    journal backup
      echo       generate a tarball of the journal directory
      echo    journal delete
      echo       delete a journal
      echo    journal help
      echo       display this help
      echo    journal list
      echo       list the journals available
      echo    journal rename
      echo       rename a journal
      echo    journal show
      echo       show the journal directory in Windows Explorer
      echo    journal TOPIC
      echo       create/update a custom journal
      echo    journal MULTI WORD TOPIC
      echo       a custom journal with multiple words will automatically
      echo       replace the spaces with underscores
      echo.
      echo Journal directory in use: 
      echo    %JDIR%
      exit/b
      )

if /i "%param%" == "list" (
      for /f %%x in ('dir /b %JDIR%') do echo %%~nx
      exit/b
      )

if /i "%param%" == "show" (
      explorer "%JDIR%"
      exit/b
      )

if /i "%param%" == "rename" (
      call :RENAMEJOURNAL
      exit/b
      )

if /i "%param%" == "delete" (
      call :DELETEJOURNAL
      exit/b
      )

if /i "%param%" == "backup" (
      call :BACKUPJOURNAL
      exit/b
      )


rem Build the name of the journal file
set dt=%DATE%
set yr=%dt:~10,4%
set mo=%dt:~4,2%
if "%mo:~0,1%" == " " set mo=0%dt:~5,1%
set bname=%yr%%mo%
if not "%param%" == "" set bname=%param%

set jfile=%JDIR%\%bname%.md

if not exist "%jfile%" echo. > "%jfile%"

rem Final error checking
if not exist "%jfile%" (
      echo The journal "%jfile%" could not be created
      exit/b 1
      )

rem Append the horizontal line and date/time line
if "%param%" == "" (
>> "%jfile%" echo.
>> "%jfile%" echo %DATE% %TIME%
>> "%jfile%" echo --------------------------------------------------
>> "%jfile%" echo.
>> "%jfile%" echo.
)

rem When using gvim, move the cursor to the end. YMMV
echo %EDITOR% | findstr /i /c:"gvim" 1>nul
if ERRORLEVEL 1 (
   start "Journal entry" "%EDITOR%" "%jfile%"
) else (
   start "Journal entry" "%EDITOR%" +  "%jfile%"
)

echo %CMDCMDLINE% | find /i "/c" >nul
if ERRORLEVEL 1 exit/b
pause
exit/b
rem ---------- end ----------


rem ------------------------------
:SW
echo %1
echo %CMDCMDLINE% | find /i "/c" >nul
if ERRORLEVEL 1 exit/b 1
pause
exit/b 1


rem ------------------------------
:RENAMEJOURNAL
for /f %%x in ('dir /b %JDIR%') do echo %%~nx
echo.
set/p oldname="Enter the name of the journal to rename: "
set oldname=%oldname: =-%
if not exist "%JDIR%\%oldname%.md" (
      echo fail
      goto :EOF
      )
set/p newname="Enter the new name: "
if "%newname%" == "" (
      echo fail
      goto :EOF
      )
set newname=%newname: =-%
if exist "%JDIR%\%newname%.md" (
      echo fail
      goto :EOF
      )
move "%JDIR%\%oldname%.md" "%JDIR%\%newname%.md"
if ERRORLEVEL 1 (
      echo fail
      goto :EOF
      )
echo success
goto :EOF


rem ------------------------------
:DELETEJOURNAL
for /f %%x in ('dir /b %JDIR%') do echo %%~nx
echo.
set/p jname="Enter the name of the journal to delete: "
set jname=%jname: =-%
if not exist "%JDIR%\%jname%.md" (
      echo fail
      goto :EOF
      )
del /p "%JDIR%\%jname%.md"
if ERRORLEVEL 1 (
      echo fail
      goto :EOF
      )
echo success
goto :EOF

rem ------------------------------
:BACKUPJOURNAL
where tar.exe
if ERRORLEVEL 1 (
      echo tar command not found, backup unavailable
      goto :EOF
      )
where gzip.exe
if ERRORLEVEL 1 (
      echo gzip command not found, backup unavailable
      goto :EOF
      )
set dt=%DATE%
set ti=%TIME%
set mo=%dt:~4,2%
set dy=%dt:~7,2%
set yr=%dt:~10,4%
set hr=%ti:~0,2%
set mn=%ti:~3,2%
if "%hr:~0,1%" == " " set hr=0%hr:~1,1%
set bkpname=journal-%yr%%mo%%dy%_%hr%%mn%
set BKPLOC=%CD%
pushd "%APPDATA%\%USERNAME%"
tar cf "%BKPLOC%\%bkpname%.tar" journal
if ERRORLEVEL 1 goto :EOF
popd
timeout /t 3
gzip %bkpname%.tar
if ERRORLEVEL 1 goto :EOF
timeout /t 3
move %bkpname%.tar.gz %bkpname%.tgz
echo success
goto :EOF
