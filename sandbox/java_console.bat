@echo off
cls
color 0a
echo.
echo Using %0
rem Reset the path to something minimal.  This is necessary
rem since some paths include '(x86)', which has caused issues at times.
set PATH=%SystemRoot%\System32;%SystemRoot%;%SystemRoot%\System32\Wbem

rem ==================================================
rem TODO adjust this path as necessary
rem pushd "%ProgramFiles%\Java\jre1.8.0_111\bin" 2>nul
pushd "%ProgramFiles%\Java\jdk1.8.0_144\bin" 2>nul
if ERRORLEVEL 1 (
      echo [-] Java not found
      goto :UTILSETUP
      )
set PATH=%CD%;%PATH%
popd

where java 1>nul
if ERRORLEVEL 1 (
      echo [-] Java command not found
      goto :UTILSETUP
      )
echo.
java -version

:UTILSETUP
set PATH=%CD%;%PATH%
if exist add_innosetup.bat call add_innosetup.bat
if exist add_vim.bat call add_vim.bat
if exist add_7zip.bat call add_7zip.bat
if exist add_bc.bat call add_bc.bat
if exist add_axcrypt.bat call add_axcrypt.bat
if exist add_doxygen.bat call add_doxygen.bat
if exist add_cvs.bat call add_cvs.bat

rem Since HOME could be a different drive, it's
rem kind of nice when 'C:' is typed, to drop into
rem your own user profile.
pushd "%USERPROFILE%"
popd

echo.
if exist "%HOME%" (
      echo HOME = %HOME%
      cd /d "%HOME%"
      ) else (
      echo USERPROFILE = %USERPROFILE%
      cd /d "%USERPROFILE%"
      )
if exist "%EDITOR%" echo EDITOR = %EDITOR%     

echo.
prompt $_$P$_$+$G$S

