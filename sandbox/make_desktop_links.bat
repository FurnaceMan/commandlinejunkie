@echo off
setlocal

set arg0=%~dp0
set arg0=%arg0:~0,-1%
if not "%arg0%" == "%CD%" (
      echo Run this script from where it sits
      exit/b 1
      )

if not exist "qt4_console.bat" (
      echo.
      echo qt4_console.bat not found
      goto :QT5SETUP
      )

echo.
echo ------------------------------------------------------------
echo Creating a desktop shortcut to the Qt4 console script
> doit.vbs echo.
>> doit.vbs echo Set oWS = WScript.CreateObject("WScript.Shell")
>> doit.vbs echo sLinkFile = "%HOMEDRIVE%%HOMEPATH%\Desktop\Qt4 Console.lnk"
>> doit.vbs echo Set oLink = oWS.CreateShortcut(sLinkFile)
>> doit.vbs echo oLink.WorkingDirectory = "%CD%"
>> doit.vbs echo oLink.TargetPath = "%ComSpec%"
>> doit.vbs echo oLink.Arguments = "/A /K ""%CD%\qt4_console.bat"""
>> doit.vbs echo oLink.Save
cscript doit.vbs
if ERRORLEVEL 1 (
      echo Unable to run Qt4 doit.vbs script
      exit/b 1
      )
del doit.vbs

:QT5SETUP
if not exist "qt5_console.bat" (
      echo.
      echo qt5_console.bat not found
      goto :JAVASETUP
      )

echo.
echo ------------------------------------------------------------
echo Creating a desktop shortcut to the Qt5 console script
> doit.vbs echo.
>> doit.vbs echo Set oWS = WScript.CreateObject("WScript.Shell")
>> doit.vbs echo sLinkFile = "%HOMEDRIVE%%HOMEPATH%\Desktop\Qt5 Console.lnk"
>> doit.vbs echo Set oLink = oWS.CreateShortcut(sLinkFile)
>> doit.vbs echo oLink.WorkingDirectory = "%CD%"
>> doit.vbs echo oLink.TargetPath = "%ComSpec%"
>> doit.vbs echo oLink.Arguments = "/A /K ""%CD%\qt5_console.bat"""
>> doit.vbs echo oLink.Save
cscript doit.vbs
if ERRORLEVEL 1 (
      echo Unable to run Qt5 doit.vbs script
      exit/b 1
      )
del doit.vbs

:JAVASETUP
if not exist "java_console.bat" (
      echo.
      echo java_console.bat not found
      goto :THE_END
      )

echo.
echo ------------------------------------------------------------
echo Creating a desktop shortcut to the Java console script
> doit.vbs echo.
>> doit.vbs echo Set oWS = WScript.CreateObject("WScript.Shell")
>> doit.vbs echo sLinkFile = "%HOMEDRIVE%%HOMEPATH%\Desktop\Java Console.lnk"
>> doit.vbs echo Set oLink = oWS.CreateShortcut(sLinkFile)
>> doit.vbs echo oLink.WorkingDirectory = "%CD%"
>> doit.vbs echo oLink.TargetPath = "%ComSpec%"
>> doit.vbs echo oLink.Arguments = "/A /K ""%CD%\java_console.bat"""
>> doit.vbs echo oLink.Save
cscript doit.vbs
if ERRORLEVEL 1 (
      echo Unable to run Java doit.vbs script
      exit/b 1
      )
del doit.vbs

:THE_END
exit/b

