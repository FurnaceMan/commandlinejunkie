@echo off
setlocal
echo.

if "%~1" == "" (
      echo requires a file argument
      goto :EOF
      )

rem Set the EDITOR
if not exist "%EDITOR%" set EDITOR=%ProgramFiles%\Windows NT\Accessories\wordpad.exe
if not exist "%EDITOR%" (
      echo no editor
      goto :EOF
      )

rem Append the date and time to the file
>> %1 echo %DATE% %TIME%
if ERRORLEVEL 1 goto :EOF

rem When using gvim, move the cursor to the end. YMMV
echo %EDITOR% | findstr /i /c:"gvim" 1>nul
if ERRORLEVEL 1 (
   start "Log entry" "%EDITOR%" %1
) else (
   start "Log entry" "%EDITOR%" + %1
)

