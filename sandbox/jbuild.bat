@echo off
setlocal ENABLEDELAYEDEXPANSION

rem Usage: jbuild [FILENAME][.pro]
rem
rem Use this script when the code is ready to be put into an installer
rem and released.
rem
rem While a simple "qmake & mingw32-make" works just about as good, this
rem script adds a few more capabilities:
rem
rem * automatically uses the right project file if there are more than one
rem   but one is named the same as the parent directory
rem * displays a warning and exits if no project file exists, or if two or
rem   more exist and one isn't the same name as the parent directory
rem * performs a 'distclean' and removes the debug and release subdirectories
rem   for a special kind of clean
rem * if the file pversion.bat exists, creates appropriate
rem   pversion.pri and pversion.iss files so that the resulting
rem   executable and installer will have consistent appropriate version numbers
rem * increments the build number of the version id so that successive calls
rem   to this script will be apparent
rem * if the InnoSetup file .iss for the appropriate application name exists
rem   collect the necessary DLLs into the release directory and then create
rem   an installer
rem * display a time of completion

rem Find the right project file
rem ----------------------------
if "%~1" == "" goto :FINDPRO

if exist "%~1.pro" (
      set PRONAME=%~1
      goto :FOUNDPRO
      )

for %%i in ("%~1") do set FEXT=%%~xi
if /i not "%FEXT%" == "pro" (
      echo !!! Not a project file, cannot continue
      goto :EOF
      )

if exist "%~1" (
      for %%x in ("%~1") do set PRONAME=%%~nx
      goto :FOUNDPRO
      )

echo !!! %~1 not found, cannot continue
goto :EOF


:FINDPRO
rem Determine the target name, according to how Qt works
rem ----------------------------------------------------------------
rem Look at all of the .pro files.
rem If one is named the same as the parent directory, use it.
rem Otherwise, use the last one found.
for %%x in ("%CD%") do set PARENTDIR=%%~nx
set procount=0
for /f %%x in ('dir/b *.pro') do (
      set PRONAME=%%~nx
      set /a procount=!procount!+1
      if /i "%%~nx" == "%PARENTDIR%" goto :FOUNDPRO
      )
if %procount% EQU 0 (
      echo.
      echo !!! No project files, cannot continue
      goto :EOF
      )
if %procount% GTR 1 (
      echo.
      echo !!! Multiple project files, cannot continue
      goto :EOF
      )

:FOUNDPRO
rem Read through the .pro file and look for a TARGET= line.
rem If found, use as the target name,
rem otherwise use the project file name as the target.
for /f "tokens=*" %%x in ('findstr /r /c:"TARGET *= *[a-z0-9_-]* *$" %PRONAME%.pro') do set TARGLINE=%%x
if "%TARGLINE%" == "" (
      APPNAME=%PRONAME%
      goto :STARTPBUILD
      )
set TARGLINE=%TARGLINE: =%
for /f "tokens=1,2 delims== " %%a in ("%TARGLINE%") do set APPNAME=%%b

:STARTPBUILD
rem Nice to have, but only if TARGET is defined as something funky,
rem like a function, or something like that.
rem choice /t 7 /c YN /d Y /m "Ok to use '%APPNAME%' as the application name"
rem if ERRORLEVEL 2 (
rem       echo Please revise %PRONAME%.pro with a clear TARGET name.
rem       goto :EOF
rem       )

rem Make sure the required tools are available
rem ------------------------------------------
for %%i in (qmake mingw32-make) do (
   where %%i 1>nul
   if ERRORLEVEL 1 (
      echo !!! %%i not found, cannot continue
      goto :EOF
      )
   )

rem Get a starting time
rem -------------------
set ti=%TIME%
set hr=%ti:~0,2%
set mn=%ti:~3,2%
if "%mn:~0,1%" == "0" set mn=%mn:~1,1%
set sc=%ti:~6,2%
if "%sc:~0,1%" == "0" set sc=%sc:~1,1%
set /a st_secs=(%hr%*3600)+(%mn%*60)+%sc%


rem Make pversion.pro and pversion.iss files if necessary
rem Clean up things a bit
rem Bump the build number if necessary
rem ---------------------
if exist pversion.bat call :MAKEVERSION
qmake %PRONAME%.pro & mingw32-make distclean & rmdir /s /q debug release
if exist pversion.bat call :BUMPBUILD


rem Start building the application
rem ------------------------------
if exist pversion.bat (
      echo.
      echo Building %APPNAME% v%PMAJOR%.%PMINOR%.%PPATCH%.%PBUILD%
      timeout /t 2
      )
qmake %PRONAME%.pro & mingw32-make --jobs=6
if ERRORLEVEL 1 goto :EOF


rem Build the installer
rem -------------------
if not exist %APPNAME%.iss goto :COMPLETION

for %%i in (depends cut compil32) do (
   where %%i 1>nul
   if ERRORLEVEL 1 (
      echo !!! %%i not found, cannot build installer
      goto :COMPLETION
      )
   )

echo.
echo Collecting DLLs, please wait...
pushd release
depends /c /pl:1 /f:1 /oc dll.csv %APPNAME%.exe
timeout /t 5
(call )>>dll.csv
if ERRORLEVEL 1 timeout /t 10
cut -d, -f2 dll.csv | findstr /r "\\qt.*\.DLL" > dll.txt
for /f %%x in (dll.txt) do xcopy /d /y "%%x" .
popd
echo.
compil32/cc %APPNAME%.iss


rem Display the completion time
rem ---------------------------
:COMPLETION
set ti=%TIME%
set hr=%ti:~0,2%
set mn=%ti:~3,2%
if "%mn:~0,1%" == "0" set mn=%mn:~1,1%
set sc=%ti:~6,2%
if "%sc:~0,1%" == "0" set sc=%sc:~1,1%
set /a tt_secs=((%hr%*3600)+(%mn%*60)+%sc%)-%st_secs%
echo.
echo Completed in %tt_secs% seconds
goto :EOF


rem ==================================================
:BUMPBUILD
set /a PBUILD=%PBUILD%+1
> pversion.bat echo set PMAJOR=%PMAJOR%
>> pversion.bat echo set PMINOR=%PMINOR%
>> pversion.bat echo set PPATCH=%PPATCH%
>> pversion.bat echo set PBUILD=%PBUILD%
call :MAKEVERSION
goto :EOF

rem ==================================================
:MAKEVERSION
call pversion.bat
> pversion.iss echo #define pVersion "%PMAJOR%.%PMINOR%.%PPATCH%.%PBUILD%"
> pversion.pri echo VERSION = %PMAJOR%.%PMINOR%.%PPATCH%.%PBUILD%
>> pversion.pri echo DEFINES += pVersion=\\\"$$VERSION\\\"
goto :EOF

