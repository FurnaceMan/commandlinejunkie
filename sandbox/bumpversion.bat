@echo off
setlocal

rem usage:  bumpversion [new|show|major|minor|patch|build]
rem
rem Generates / updates the buildversion.bat, .h, .iss, .pri files.
rem
rem Use this script with 'new' to create a new buildversion.bat file.  The
rem buildversion.bat file sets the VERSION_MAJOR, VERSION_MINOR, VERSION_PATCH
rem and VERSION_BUILD environment variables.
rem
rem Use 'show' to simply display the current version.
rem
rem Use 'major', 'minor', 'patch' or 'build' to increment the appropriate digit
rem of the version id in the buildversion.bat file and then re-create the
rem associated buildversion.* files, depending on whether they exist.

if /i "%~1" == "new" (
      if exist buildversion.bat (
         echo bumpversion error: can't make new, buildversion.bat exists
         exit/b 1
      )
      set VERSION_MAJOR=0
      set VERSION_MINOR=1
      set VERSION_PATCH=0
      set VERSION_BUILD=0
> buildversion.bat echo.
> buildversion.h echo.
> buildversion.iss echo.
> buildversion.pri echo.
      goto :MAKEVERSION
      )

rem not having a buildversion isn't necessarily an error
if not exist buildversion.bat exit/b 0

call buildversion.bat

if /i "%~1" == "show" goto :SHOWVERSION

if /i "%~1" == "major" (
      set /a VERSION_MAJOR=%VERSION_MAJOR%+1
      set VERSION_MINOR=0
      set VERSION_PATCH=0
      set VERSION_BUILD=0
      goto :MAKEVERSION
      )

if /i "%~1" == "minor" (
      set /a VERSION_MINOR=%VERSION_MINOR%+1
      set VERSION_PATCH=0
      set VERSION_BUILD=0
      goto :MAKEVERSION
      )

if /i "%~1" == "patch" (
      set /a VERSION_PATCH=%VERSION_PATCH%+1
      set VERSION_BUILD=0
      goto :MAKEVERSION
      )

if /i "%~1" == "build" (
      set /a VERSION_BUILD=%VERSION_BUILD%+1
      goto :MAKEVERSION
      )

echo usage:  bumpversion [new show major minor patch build]
exit/b 1

:MAKEVERSION

> buildversion.bat ((echo set VERSION_MAJOR=%VERSION_MAJOR%) & (echo set VERSION_MINOR=%VERSION_MINOR%) & (echo set VERSION_PATCH=%VERSION_PATCH%) & (echo set VERSION_BUILD=%VERSION_BUILD%))

if exist buildversion.iss (
> buildversion.iss ((echo #define BuildVersion "%VERSION_MAJOR%.%VERSION_MINOR%.%VERSION_PATCH%.%VERSION_BUILD%") & (echo #define PatchVersion "%VERSION_MAJOR%.%VERSION_MINOR%.%VERSION_PATCH%"))
)

if exist buildversion.pri (
> buildversion.pri (echo VERSION = %VERSION_MAJOR%.%VERSION_MINOR%.%VERSION_PATCH%.%VERSION_BUILD%)
)

if exist buildversion.h (
> buildversion.h ((echo #ifndef BuildVersion) & (echo #define BuildVersion "%VERSION_MAJOR%.%VERSION_MINOR%.%VERSION_PATCH%.%VERSION_BUILD%") & (echo #endif))
)

:SHOWVERSION
echo.
echo %VERSION_MAJOR%.%VERSION_MINOR%.%VERSION_PATCH%.%VERSION_BUILD%

exit/b 0
