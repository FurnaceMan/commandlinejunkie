@echo off
setlocal
echo.

rem ------------------------------------------------------------
rem Create a Markdown file under the USERPROFILE/Google Drive/DriveNote
rem directory with the date code name of the form 'yyyyMM' (for example,
rem '201609.md').  Then append a horizontal line with a date and time below it
rem and then send it to an EDITOR so that the user can freely enter
rem information.
rem
rem With no arguments, this script creates/adds to a note file with the
rem current date code.
rem
rem The 'list' argument lists all of the notes available.
rem
rem The 'delete' argument lets the user delete a note
rem
rem The 'rename' argument lets the user rename a note
rem
rem Any other argument is taken as the base name of the note file.  No date
rem code is appended, the user is put into the editor for that file.
rem ------------------------------------------------------------

set JTOP=%USERPROFILE%\Google Drive
set JSUB=DriveNote

pushd "%JTOP%"
if ERRORLEVEL 1 goto :SW "Install Google Drive first"
if not exist "%JSUB%" (
      mkdir "%JSUB%"
      attrib +h "%JSUB%"
      )
popd

pushd "%JTOP%\%JSUB%"
if ERRORLEVEL 1 goto :SW "Could not create the %JSUB% directory under %JTOP%"
set JDIR=%CD%
popd


rem Remove leading dashes for those who are used to unix-like options
set param=%*
if "%param:~0,2%" == "--" (
      set param=%param:~2%
      )
if "%param:~0,1%" == "-" (
      set param=%param:~1%
      )

rem Remove spaces from the parameter
if not "%param%" == "" set param=%param: =-%


rem Set the EDITOR
if not exist "%EDITOR%" set EDITOR=%ProgramFiles%\Windows NT\Accessories\wordpad.exe
if not exist "%EDITOR%" goto :SW "EDITOR is not defined properly"


if /i "%param%" == "help" (
      echo DriveNote v2.0.0
      echo.
      echo usage:
      echo    drivenote
      echo      * generate a default date-coded note
      echo    drivenote delete
      echo      * delete a note
      echo    drivenote help
      echo      * show this help
      echo    drivenote list
      echo      * list available notes
      echo    drivenote rename
      echo      * rename a note
      echo    drivenote show
      echo      * show the notes in explorer
      echo    drivenote TOPIC
      echo    drivenote MULTI WORD TOPIC
      echo      * generate a note on a TOPIC or MULTI WORD TOPIC
      echo.
      echo directory: 
      echo    %JDIR%
      exit/b
      )

if /i "%param%" == "list" (
      for /f %%x in ('dir /b "%JDIR%"') do echo %%~nx
      exit/b
      )

if /i "%param%" == "show" (
      explorer "%JDIR%"
      exit/b
      )

if /i "%param%" == "rename" (
      call :RENAMENOTE
      exit/b
      )

if /i "%param%" == "delete" (
      call :DELETENOTE
      exit/b
      )


rem Build the name of the note file
set dt=%DATE%
set yr=%dt:~10,4%
set mo=%dt:~4,2%
if "%mo:~0,1%" == " " set mo=0%dt:~5,1%
set bname=%yr%%mo%
if not "%param%" == "" set bname=%param%

set jfile=%JDIR%\%bname%.md

if not exist "%jfile%" echo. > "%jfile%"

rem Final error checking
if not exist "%jfile%" (
      echo "%jfile%" could not be created
      exit/b 1
      )

rem Append the horizontal line and date/time line
if "%param%" == "" (
>> "%jfile%" echo.
>> "%jfile%" echo %DATE% %TIME%
>> "%jfile%" echo --------------------------------------------------
>> "%jfile%" echo.
>> "%jfile%" echo.
)

rem When using gvim, move the cursor to the end. YMMV
echo %EDITOR% | findstr /i /c:"gvim" 1>nul
if ERRORLEVEL 1 (
   start "DriveNote entry" "%EDITOR%" "%jfile%"
) else (
   start "DriveNote entry" "%EDITOR%" +  "%jfile%"
)

exit/b
rem ---------- end ----------

:SW
rem Something Wrong
echo %1
exit/b 1

rem ------------------------------
:RENAMENOTE
for /f %%x in ('dir /b "%JDIR%"') do echo %%~nx
echo.
set/p oldname="Enter the name of the note to rename: "
set oldname=%oldname: =-%
if not exist "%JDIR%\%oldname%.md" (
      echo fail
      goto :EOF
      )
set/p newname="Enter the new name: "
if "%newname%" == "" (
      echo fail
      goto :EOF
      )
set newname=%newname: =-%
if exist "%JDIR%\%newname%.md" (
      echo fail
      goto :EOF
      )
move "%JDIR%\%oldname%.md" "%JDIR%\%newname%.md"
if ERRORLEVEL 1 (
      echo fail
      goto :EOF
      )
echo success
goto :EOF


rem ------------------------------
:DELETENOTE
for /f %%x in ('dir /b "%JDIR%"') do echo %%~nx
echo.
set/p jname="Enter the name of the note to delete: "
set jname=%jname: =-%
if not exist "%JDIR%\%jname%.md" (
      echo fail
      goto :EOF
      )
del /p "%JDIR%\%jname%.md"
if ERRORLEVEL 1 (
      echo fail
      goto :EOF
      )
echo success
goto :EOF
