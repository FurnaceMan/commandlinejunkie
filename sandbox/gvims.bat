@echo off

if exist "%~n1.ui" start /b designer "%~n1.ui"

if exist "%~n1.cpp" if exist "%~n1.h" start /b gvim -o2 "%~n1.h" "%~n1.cpp" & exit/b

if exist "%~n1.cpp" start /b gvim "%~n1.cpp" & exit/b

if exist "%~n1.c" if exist "%~n1.h" start /b gvim -o2 "%~n1.h" "%~n1.c" & exit/b

if exist "%~n1.c" start /b gvim "%~n1.c" & exit/b

