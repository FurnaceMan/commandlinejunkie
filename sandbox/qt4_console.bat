@echo off
cls
echo.
echo Using %0
rem Reset the path to something minimal.  This is necessary
rem since some paths include '(x86)', which has caused issues at times.
set PATH=%SystemRoot%\System32;%SystemRoot%;%SystemRoot%\System32\Wbem

rem ==================================================
rem TODO adjust as necessary
pushd "%SystemDrive%\Qt\4.8.6\bin" 2>nul
if ERRORLEVEL 1 (
      echo [-] Qt environment not found
      goto :UTILSETUP
      )
call qtvars.bat
rem Qt4 resets the path and forgets to add thse on
set PATH=%PATH%;%SystemRoot%;%SystemRoot%\System32\Wbem
popd

where qmake 1>nul
if ERRORLEVEL 1 (
      echo [-] Qt qmake command not found
      goto :UTILSETUP
      )
echo.
qmake --version

:UTILSETUP
set PATH=%CD%;%PATH%
pushd ..\wbin
if ERRORLEVEL 1 (
      echo !!! wbin directory not found
      goto :COMPLETION
      )
set PATH=%PATH%;%CD%
if exist add_innosetup.bat call add_innosetup.bat
if exist add_vim.bat call add_vim.bat
if exist add_7zip.bat call add_7zip.bat
if exist add_bc.bat call add_bc.bat
if exist add_axcrypt.bat call add_axcrypt.bat
if exist add_doxygen.bat call add_doxygen.bat
if exist add_cvs.bat call add_cvs.bat
popd

:COMPLETION
echo.
if exist "%HOME%" (
      echo HOME = %HOME%
      cd /d "%HOME%"
      ) else (
      echo USERPROFILE = %USERPROFILE%
      cd /d "%USERPROFILE%"
      )
if exist "%EDITOR%" echo EDITOR = %EDITOR%     

echo.
prompt $_$P$_$+$G$S

