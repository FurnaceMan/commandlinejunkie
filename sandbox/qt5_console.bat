@echo off
cls
echo.
echo Using %0
rem Reset the path to something minimal.  This is necessary
rem since some paths include '(x86)', which has caused issues at times.
set PATH=%SystemRoot%\System32;%SystemRoot%;%SystemRoot%\System32\Wbem

rem ==================================================
rem TODO adjust as necessary
pushd "%SystemDrive%\Qt\qt5.8.0\5.8\mingw53_32\bin" 2>nul
rem pushd "%SystemDrive%\Qt\qt5.7.0\5.7\mingw53_32\bin" 2>nul
rem pushd "%SystemDrive%\Qt\qt5.6.0\5.6\mingw49_32\bin" 2>nul
if ERRORLEVEL 1 (
      echo [-] Qt environment not found
      goto :UTILSETUP
      )
rem Don't call qtenv2.bat for Qt5.8.0 since it changes the path
rem using forward slashes, which should not be used in Windows.
rem      call qtenv2.bat
set PATH=C:\Qt\Qt5.8.0\5.8\mingw53_32\bin;C:\Qt\Qt5.8.0\Tools\mingw530_32\bin;%PATH%
popd

where qmake 1>nul
if ERRORLEVEL 1 (
      echo [-] Qt qmake command not found
      goto :UTILSETUP
      )
echo.
qmake --version

:UTILSETUP
set PATH=%CD%;%PATH%
pushd ..\wbin
if ERRORLEVEL 1 (
      echo !!! wbin directory not found, some commands may not be available
      goto :COMPLETION
      )
set PATH=%PATH%;%CD%
if exist add_perl.bat call add_perl.bat
if exist add_innosetup.bat call add_innosetup.bat
if exist add_vim.bat call add_vim.bat
if exist add_7zip.bat call add_7zip.bat
if exist add_bc.bat call add_bc.bat
if exist add_axcrypt.bat call add_axcrypt.bat
if exist add_doxygen.bat call add_doxygen.bat
if exist add_cvs.bat call add_cvs.bat
popd

:COMPLETION
echo.
if defined HOME (
      echo HOME = %HOME%
      if not exist "%HOME%" (
         echo !!! HOME is not defined correctly
         ) else (
         cd /d "%HOME%"
         )
      ) else (
      echo USERPROFILE = %USERPROFILE%
      cd /d "%USERPROFILE%"
      )
if defined EDITOR echo EDITOR = %EDITOR%
if defined TARBALLDIR echo TARBALLDIR = %TARBALLDIR%

echo.
prompt $_$P$_$+$G$S

