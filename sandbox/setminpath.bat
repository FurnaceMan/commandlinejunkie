@echo off

rem Reset the path to something minimal.  This is necessary
rem since some paths include '(x86)', which has caused issues at times.

set PATH=%SystemRoot%\System32;%SystemRoot%;%SystemRoot%\System32\Wbem

