@echo off

rem Run this script to deploy desired files to the HOMEBIN directory.
rem Make sure to re-run this if the deploylist.txt file has been changed.

set arg0=%~dp0
set arg0=%arg0:~0,-1%
if "%arg0%" NEQ "%CD%" (
      echo.
      echo This script must be run in its own directory
      goto :ENDPAUSE
      )

rem Usually HOMEBIN is defined as %HOME%\bin\win
if not exist "%HOMEBIN%" (
      echo.
      echo HOMEBIN is not defined, see winbat repository
      goto :ENDPAUSE
      )

echo Creating temporary tarball...
copy utils.tgz tmp.tgz > nul

echo Unzipping the tarball...
.\gunzip.exe tmp.tgz

echo Extracting utils from the tar...
.\tar.exe xf tmp.tar

echo Copying utilities...
for /f %%i in (deploylist.txt) do xcopy /d /y utils\%%i "%HOMEBIN%"

echo Deleting the temporary tarball
del tmp.tar

rem echo.
rem set /p response="Delete utils subdirectory? (y/n) "
rem if /i "%response:~0,1%" == "y" (
   echo Deleting the utils subdirectory
   rmdir /s /q utils
rem )

:ENDPAUSE
echo.
pause

