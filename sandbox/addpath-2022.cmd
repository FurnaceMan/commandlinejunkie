@echo off
rem ------------------------------------------------------------
rem Append/prepend a directory to the user PATH variable.
rem Includes 'keywords' that coorespond to commonly used directories.
rem ------------------------------------------------------------

rem Set up a temporary BAT filename that gets called outside of the
rem setlocal scope
if not exist "%LOCALAPPDATA%\qtclu\transient" (
      pushd %LOCALAPPDATA%
      mkdir qtclu\transient
      popd
      )
set "tmpfile=%LOCALAPPDATA%\qtclu\transient\setnewpath.bat"


setlocal ENABLEEXTENSIONS ENABLEDELAYEDEXPANSION


rem At least one command parameter is required, otherwise show help.
if "%~1" == "" goto :USAGE

rem If this script is named 'prepend.bat' then prepend the directory
rem instead of appending it to the PATH.
set prepend=0
if /i "%~nx0" == "prepath.bat" set prepend=1

rem Get the directory name from the command line
set "dirname=%~1"

rem Some tools, Qt in particular, require multiple directories. The suppdir
rem variable, if set, is also appended/prepended.
set suppdir=

rem ------------------------------------------------------------
rem TODO: Adjust keywords and paths to taste, then rebuild installer.
rem       Set prepend to one to force prepending even when addpath is used.
rem
rem       Remember to update USAGE if something is added/removed here.
rem ------------------------------------------------------------

rem - - - - - - - - - - - - - - - - - - - - 
rem 7zip
if /i "%~1" == "7zip" set "dirname=%ProgramFiles%\7-zip"

rem - - - - - - - - - - - - - - - - - - - - 
rem AxCrypt
if /i "%~1" == "axcrypt" set "dirname=%ProgramFiles%\Axantum\AxCrypt"

rem - - - - - - - - - - - - - - - - - - - - 
rem Beyond Compare
if /i "%~1" == "bc2" set "dirname=%ProgramFiles(x86)%\beyond compare 2"

rem - - - - - - - - - - - - - - - - - - - - 
rem Doxygen
if /i "%~1" == "doxygen" set "dirname=%ProgramFiles%\doxygen\bin"

rem - - - - - - - - - - - - - - - - - - - - 
rem InnoSetup
if /i "%~1" == "inno" set "dirname=%ProgramFiles(x86)%\Inno Setup 5"

rem - - - - - - - - - - - - - - - - - - - - 
rem Java Development
if /i "%~1" == "jdk" (
      set "dirname=%ProgramFiles%\Java\jdk1.8.0_201\bin"
      set prepend=1
      )

rem - - - - - - - - - - - - - - - - - - - - 
rem Java Runtime
if /i "%~1" == "jre" (
      set "dirname=%ProgramFiles%\Java\jre1.8.0_201\bin"
      set prepend=1
      )

rem - - - - - - - - - - - - - - - - - - - - 
rem Perl
if /i "%~1" == "perl" set dirname=%SystemDrive%\Perl64\bin

rem - - - - - - - - - - - - - - - - - - - - 
rem Windows PowerShell
if /i "%~1" == "psh" set "dirname=%SystemRoot%\System32\WindowsPowerShell\v1.0"

rem - - - - - - - - - - - - - - - - - - - - 
rem Python
if /i "%~1" == "python" set "dirname=%LOCALAPPDATA%\Programs\Python\Python37-32"

rem - - - - - - - - - - - - - - - - - - - - 
rem Qt Development Framework, version 5
rem - qtenv2.bat erroneously uses forward slashes, do things manually
if /i "%~1" == "qt5" (
         set dirname=%SystemDrive%\Qt\Qt5.11.1\5.11.1\mingw53_32\bin
         set suppdir=%SystemDrive%\Qt\Qt5.11.1\Tools\mingw530_32\bin
rem         set dirname=%SystemDrive%\Qt\Qt5.11.2\5.11.2\mingw53_32\bin
rem         set suppdir=%SystemDrive%\Qt\Qt5.11.2\Tools\mingw530_32\bin
      )

rem - - - - - - - - - - - - - - - - - - - - 
rem Vim (gVim)
if /i "%~1" == "vim" set "dirname=%ProgramFiles(x86)%\vim\vim81"

rem end of keywords
rem ------------------------------------------------------------

rem Create the temporary script that reworks the PATH
if exist "%tmpfile%" del "%tmpfile%"
if exist "%dirname%" (
   if %prepend% EQU 1 (
> "%tmpfile%" echo echo [+] ^^^< %~1
   ) else (
> "%tmpfile%" echo echo [+] ^^^> %~1
   )
) else (
> "%tmpfile%" echo echo [-] %~1
>> "%tmpfile%" echo exit/b 1
)
if defined suppdir (
   if %prepend% EQU 1 (
>> "%tmpfile%" echo set "PATH=%dirname%;%suppdir%;%%PATH%%"
   ) else (
>> "%tmpfile%" echo set "PATH=%%PATH%%;%dirname%;%suppdir%"
   )
) else (
   if %prepend% EQU 1 (
>> "%tmpfile%" echo set "PATH=%dirname%;%%PATH%%"
   ) else (
>> "%tmpfile%" echo set "PATH=%%PATH%%;%dirname%"
   )
)
>> "%tmpfile%" echo exit/b 0

endlocal

rem Outside of setlocal scope, call the temporary script.
call "%tmpfile%"
set ecode=%ERRORLEVEL%
set tmpfile=
exit/b %ecode%


:USAGE
rem Use addpath/prepath with no arguments to display usage
set tmpfile=
echo.
echo usage: addpath^|prepath [KEYWORD^|DIRECTORY]
echo.
echo Keywords:
echo    7zip       7-zip
echo    axcrypt    AxCrypt
echo    bc2        Beyond Compare 2
echo    doxygen    Doxygen
echo    inno       InnoSetup
echo    jdk        Java Development Kit
echo    jre        Java Runtime Environment
echo    perl       Perl
echo    psh        Windows Powershell
echo    python     Python
echo    qt5        Qt 5 Development Framework
echo    vim        gVim
exit/b 0

