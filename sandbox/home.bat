@echo off

rem Change directory to HOME, or USERPROFILE
rem
rem Unlike cdhome.bat, this simply changes directory,
rem it doesn't pushd it onto the directory stack.

if exist "%HOME%" (
   cd /d "%HOME%"
) else (
   cd /d "%USERPROFILE%"
)

