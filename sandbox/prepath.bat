@echo off

rem Prepends the given directory, or current directory, to the PATH.

if "%~1" == "" (
      set PATH=%CD%;%PATH%
      goto :EOF
      )
pushd "%~1"
if ERRORLEVEL 1 goto :EOF
set PATH=%CD%;%PATH%
popd

