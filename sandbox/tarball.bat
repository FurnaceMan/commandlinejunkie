@echo off
setlocal

rem ------------------------------------------------------------
rem tarball script
rem
rem Take given directory(ies) and tar-gzip to
rem %LOCALAPPDATA%/tarball/YYMMDD_hhmm/BASENAME.tgz
rem ------------------------------------------------------------

where tar.exe 1>nul
if ERRORLEVEL 1 (
      echo 'tar' command not found, please add it to your PATH
      exit/b 1
      )

where gzip.exe 1>nul
if ERRORLEVEL 1 (
      echo 'gzip' command not found, please add it to your PATH
      exit/b 1
      )

:GETDATETIME
set dt=%DATE%
set ti=%TIME%
set mo=%dt:~4,2%
set dy=%dt:~7,2%
set yr=%dt:~10,4%
set hr=%ti:~0,2%
set mn=%ti:~3,2%
if "%hr:~0,1%" == " " set hr=0%hr:~1,1%

set subdir=%yr%%mo%%dy%_%hr%%mn%

if not defined TARBALLDIR set TARBALLDIR=%LOCALAPPDATA%
pushd %TARBALLDIR%
if ERRORLEVEL 1 (
      echo TARBALLDIR is not defined correctly
      exit/b 1
      )
mkdir tarball\%subdir%
if ERRORLEVEL 1 (
      popd
      echo Already tarball'ing to %subdir%, waiting a minute...
      timeout /t 60
      goto :GETDATETIME
      )
cd tarball\%subdir%
set TBDIR=%CD%
popd

:TOPLINE
if "%~1" EQU "" goto :EOF
pushd %1
if ERRORLEVEL 1 goto :NEXTARG
if "%CD:~-1%" EQU "\" (
      echo Cannot tarball the root directory
      goto :NEXTPOP
      )
for %%x in ("%CD%") do set base=%%~nx%%~xx
cd ..
echo.
echo tar cf "%TBDIR%\%base%.tar" "%base%"
tar cf "%TBDIR%\%base%.tar" "%base%"
if ERRORLEVEL 1 goto :NEXTPOP
rem wait a little to ensure the file is finished writing
timeout /t 3
echo.
echo gzip "%TBDIR%\%base%.tar"
gzip "%TBDIR%\%base%.tar"
if ERRORLEVEL 1 goto :NEXTPOP
timeout /t 3
echo.
echo move "%TBDIR%\%base%.tar.gz" "%TBDIR%\%base%.tgz"
move "%TBDIR%\%base%.tar.gz" "%TBDIR%\%base%.tgz"
if ERRORLEVEL 1 goto :NEXTPOP

:NEXTPOP
popd
:NEXTARG
shift /1
goto :TOPLINE

