@echo off

rem Appends the given directory, or current directory, to the PATH.

if "%~1" == "" (
      set PATH=%PATH%;%CD%
      goto :EOF
      )
pushd "%~1"
if ERRORLEVEL 1 goto :EOF
set PATH=%PATH%;%CD%
popd

