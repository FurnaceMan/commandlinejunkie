@echo off

rem Change (pushd) directory to HOME (or USERPROFILE)
rem
rem If a subdirectory name parameter is given,
rem pushd to that subdirectory off of HOME (or USERPROFILE).

if exist "%HOME%" (
   if "%~1" == "" (
      pushd "%HOME%"
   ) else (
      pushd "%HOME%\%~1"
   )
   goto :EOF
)

if "%~1" == "" (
   pushd "%USERPROFILE%"
) else (
   pushd "%USERPROFILE%\%~1"
)

