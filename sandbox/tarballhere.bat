@echo off
setlocal

rem ------------------------------------------------------------
rem Tarball here script
rem
rem Take given directory(ies) and tar-gzip to
rem CURRENT_WORKING_DIRECTORY\BASENAME.tgz
rem
rem Unlike tarball.bat, this just tarballs a directory to
rem your current working directory.
rem
rem Includes a user query to delete the original directory
rem when complete.
rem ------------------------------------------------------------

where tar.exe 1>nul
if ERRORLEVEL 1 (
      echo 'tar' command not found, please add it to your PATH
      exit/b
      )

where gzip.exe 1>nul
if ERRORLEVEL 1 (
      echo 'gzip' command not found, please add it to your PATH
      exit/b
      )

set TARBALLDIR=%CD%

set userdel=n
:TOPLINE
if "%~1" == "" goto :EOF
pushd %1
if ERRORLEVEL 1 goto :NEXTARG
if "%CD:~-1%" == "\" (
      echo Cannot tarball the root directory
      goto :NEXTPOP
      )
for %%x in ("%CD%") do set base=%%~nx%%~xx
cd ..
if exist "%TARBALLDIR%\%base%.tar" (
      echo %base%.tar already exists
      goto :NEXTPOP
      )
if exist "%TARBALLDIR%\%base%.tar.gz" (
      echo %base%.tar.gz already exists
      goto :NEXTPOP
      )
if exist "%TARBALLDIR%\%base%.tgz" (
      echo %base%.tgz already exists
      goto :NEXTPOP
      )
echo.
echo tar cf "%TARBALLDIR%\%base%.tar" "%base%"
tar cf "%TARBALLDIR%\%base%.tar" "%base%"
if ERRORLEVEL 1 goto :NEXTPOP
rem wait a little to ensure the file is finished writing
timeout /t 3
echo.
echo gzip "%TARBALLDIR%\%base%.tar"
gzip "%TARBALLDIR%\%base%.tar"
if ERRORLEVEL 1 goto :NEXTPOP
timeout /t 3
echo.
echo move "%TARBALLDIR%\%base%.tar.gz" "%TARBALLDIR%\%base%.tgz"
move "%TARBALLDIR%\%base%.tar.gz" "%TARBALLDIR%\%base%.tgz"
if ERRORLEVEL 1 goto :NEXTPOP

echo.
set/p userdel="Delete directory %base%? (%userdel%) "
if /i "%userdel:~0,1%" == "y" (
      echo Deleting %base% ...
      rmdir /s /q "%base%"
      )

:NEXTPOP
popd
:NEXTARG
shift /1
goto :TOPLINE

