@echo off
echo.

rem A boilerplate for Windows command scripts.  Remember to 'call' this script
rem within another Windows script, or else it will exit the main script
rem prematurely.  You can use 'goto :EOF' instead of 'exit' below if you don't
rem need this script to return an error level.


rem Set up the extensions, almost no reason not to use them
setlocal ENABLEEXTENSIONS ENABLEDELAYEDEXPANSION

rem Color the terminal window to show something is running
color 0e

rem Move to the directory where this script is located
cd /d "%~dp0"

rem TODO do some stuff here, for example...
where /q qmake
if ERRORLEVEL 1 (
    echo qmake not found, set up the Qt development environment
    goto :SOMETHINGWRONG
)
qmake
if ERRORLEVEL 1 goto :SOMETHINGWRONG


:COMPLETION
rem Use the COMPLETION label when you just want to exit early without error.
echo.
echo Finished
echo.
rem return the terminal back to its original color
color
rem This will wait around for a few seconds before bailing if the script was
rem double-clicked in explorer.  Or exit immediately if run in command line.
echo %CMDCMDLINE% | find /i "/c" >nul
if ERRORLEVEL 1 exit /b 0
timeout /t 7
exit/b 0


:SOMETHINGWRONG
rem Use the SOMETHINGWRONG label when you need to exit early with an error.
echo.
rem Color the terminal window to indicate a problem ocurred
color 0c
rem Like above, except pause until the user has had time to view the error.
echo %CMDCMDLINE% | find /i "/c" >nul
if ERRORLEVEL 1 exit /b 1
pause
exit/b 1

