@echo off
setlocal
echo.

rem ------------------------------------------------------------
rem Tarunball script
rem
rem Take a given *.tgz file and extract it to the current
rem working directory.
rem ------------------------------------------------------------

if "%~1" == "" goto :EOF

where tar.exe 1>nul
if ERRORLEVEL 1 (
      echo 'tar' command not found, please add it to your PATH
      exit/b
      )

where gunzip.exe 1>nul
if ERRORLEVEL 1 (
      echo 'gunzip' command not found, please add it to your PATH
      exit/b
      )

rem TODO check for existence of FILE.tar and FILE/ directory first

set rnum=%RANDOM%
copy %1 z%rnum%.tgz
gunzip z%rnum%.tgz
tar xf z%rnum%.tar
del z%rnum%.tar

