@echo off

rem A robust script for compiling Qt programs.
rem The first command line parameter gets passed to the 'make' command.

setlocal ENABLEEXTENSIONS ENABLEDELAYEDEXPANSION
color 0e
echo.

rem Move to the directory where this script is located
cd /d "%~dp0"

rem Get the command line argument, set the default to 'release'
set "arg1=%~1"
if "%arg1%" == "" set arg1=release


if /i "%arg1:~-7%" == "install" (
    if not defined INSTALL_ROOT (
        echo INSTALL_ROOT is not defined
        goto :SOMETHINGWRONG
    )
    pushd "%INSTALL_ROOT%"
    if ERRORLEVEL 1 (
        echo %INSTALL_ROOT% does not exist
        goto :SOMETHINGWRONG
    )
    popd
)


rem Figure out whether the MinGW optionr or
rem the Visual Studio option is in play.
where/q qmake
if ERRORLEVEL 1 (
    echo qmake not found, use a Qt command window
    goto :SOMETHINGWRONG
)

where/q mingw32-make
if %ERRORLEVEL% EQU 0 (
    set MAKECMD=mingw32-make
    set "MAKEOPT=--jobs=4"
    goto :LETSDOTHIS
)

where/q nmake
if %ERRORLEVEL% EQU 0 (
    set MAKECMD=nmake
    set MAKEOPT=
    goto :LETSDOTHIS
)

echo Unable to find a suitable make utility
goto :SOMETHINGWRONG


rem ============================================================
:LETSDOTHIS

qmake
if ERRORLEVEL 1 goto :SOMETHINGWRONG
%MAKECMD% %1 2> error.log
if %ERRORLEVEL% EQU 0 goto :COMPLETION

rem An error here may mean this is waiting for some library to complete.
rem I don't have complete confidence in how DEPENDPATH works, so this is done.
if not exist depends.txt goto :SOMETHINGWRONG
where /q waitforfile
if ERRORLEVEL 1 goto :SOMETHINGWRONG

rem Wait around until all files listed in depends.txt exist
for /f "tokens=* delims=" %%d in (depends.txt) do waitforfile "%%d"

rem Now try and compile (and link) again
%MAKECMD% %1 2>> error.log
if ERRORLEVEL 1 goto :SOMETHINGWRONG


:COMPLETION
echo.
echo Finished
echo.
color
echo %CMDCMDLINE% | find /i "/c" >nul
if ERRORLEVEL 1 exit /b 0
timeout /t 7
exit/b 0


:SOMETHINGWRONG
color 0c
echo %CMDCMDLINE% | find /i "/c" >nul
if ERRORLEVEL 1 exit /b 1
pause
exit/b 1

