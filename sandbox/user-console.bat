@echo off
cls
color 0a
echo.
echo %0

rem Reset the path to something minimal since some paths include '(x86)',
rem which can cause issues.
set PATH=%SystemRoot%\System32;%SystemRoot%;%SystemRoot%\System32\Wbem

rem Prepend the current working directory to the path.
set PATH=%CD%;%PATH%

rem Allow the user to have HOME defined or just use USERPROFILE.
echo.
if exist "%HOME%" (
      echo HOME = %HOME%
      cd /d "%HOME%"
      ) else (
      echo USERPROFILE = %USERPROFILE%
      cd /d "%USERPROFILE%"
      )

rem Display the EDITOR value
if exist "%EDITOR%" echo EDITOR = %EDITOR%     

rem adjust the prompt to show the current directory
echo.
prompt $_$P$_$+$G$S

