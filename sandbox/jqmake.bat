@echo off
setlocal

rem jimmy-quick-make - qmake script that does more than qmake
rem
rem jqmake [PRO-FILE] [MAKE-TARGET]
rem
rem If both a Qt .pro file and make target are specified, the Qt .pro file
rem must be listed first.  It also must end with a .pro suffix.
rem
rem If you aleady have a hand built Makefile and don't want to use qmake,
rem then don't put a .pro file in the current directory.  See RUNMAKECMD below.
rem
rem This script assumes files will get installed to INSTALL_ROOT/bin and that
rem INSTALL_ROOT should be defined if an install target is specified.
rem
rem The following details the usage and some command equivalents:
rem
rem jqmake
rem    qmake & mingw32-make
rem
rem jqmake file.pro
rem    qmake file.pro & mingw32-make
rem jqmake file.pro install
rem    - or anything that ends with 'install'
rem    qmake file.pro & mingw32-make --jobs=6 MAKE-TARGET
rem jqmake file.pro MAKE-TARGET
rem    - example MAKE-TARGETs: debug release clean distclean
rem    qmake file.pro & mingw32-make MAKE-TARGET
rem
rem jqmake install
rem    qmake & mingw32-make --jobs=6 MAKE-TARGET
rem jqmake MAKE-TARGET
rem    qmake & mingw32-make MAKE-TARGET

set MAKECMD=mingw32-make

rem Command line processing
rem -----------------------
set PRONAME=
set MTARGET=
:CMDLINECHECK
if "%~1" == "" goto :FINISHEDCMDLINE
for /f %%i in ("%~1") do set FEXT=%%~xi
if /i "%FEXT%" == ".pro" (
      set PRONAME=%~1
      shift /1
      goto :CMDLINECHECK
      ) else (
      set MTARGET=%*
      )
:FINISHEDCMDLINE
if /i "%MTARGET%" == "" goto :CHECKPRO

rem this script prefers that INSTALL_ROOT should be defined
rem when installing and that it runs the install as multiple jobs
rem ...adjust to taste
if /i "%MTARGET:~-7%" == "install" (
      if not defined INSTALL_ROOT (
            echo jqmake error: INSTALL_ROOT should be defined
            exit/b 1
            )
      set MTARGET=--jobs=6 %MTARGET%
      )

:CHECKPRO
if not "%PRONAME%" == "" (
      if not exist "%PRONAME%" (
         echo jqmake error: %PRONAME% does not exist
         exit/b 1
         )
      )

rem Make sure the required tools are available
rem ------------------------------------------
for %%i in (%MAKECMD%) do (
   where/q %%i
   if ERRORLEVEL 1 (
      echo jqmake error: %%i not found
      exit/b 1
      )
   )

rem Get a starting time
rem -------------------
set ti=%TIME%
set hr=%ti:~0,2%
set mn=%ti:~3,2%
if "%mn:~0,1%" == "0" set mn=%mn:~1,1%
set sc=%ti:~6,2%
if "%sc:~0,1%" == "0" set sc=%sc:~1,1%
set /a st_secs=(%hr%*3600)+(%mn%*60)+%sc%

rem The following logic is kind of convoluted...

set has_pro=1
dir/b *.pro 1>nul 2>nul
if ERRORLEVEL 1 set has_pro=0

rem If no Makefile, then qmake must be run
if not exist Makefile goto :RUNQMAKE

rem if no .PRO file (but Makefile exists), then don't run qmake
if %has_pro% EQU 0 goto :RUNMAKECMD

:RUNQMAKE
where/q qmake.exe
if ERRORLEVEL 1 (
      echo jqmake error: qmake not found
      exit/b 1
      )
echo ------------------------------------------------------------
echo.
echo qmake
echo.
echo ------------------------------------------------------------
if %has_pro% EQU 1 (
   if "%PRONAME%" == "" (
      qmake
   ) else (
      qmake "%PRONAME%"
   )
) else (
   qmake --project
   qmake
)
if ERRORLEVEL 1 exit/b 1

rem run make
rem --------
:RUNMAKECMD
echo ------------------------------------------------------------
echo.
echo %MAKECMD% %MTARGET%
echo.
echo ------------------------------------------------------------
%MAKECMD% %MTARGET%
if ERRORLEVEL 1 exit/b 1

rem do some extra clean when disclean is used
rem -----------------------------------------
if /i "%MTARGET%" == "distclean" (
      if exist debug rmdir/s/q debug
      if exist release rmdir/s/q release
      goto :COMPLETION
      )

rem collect necessary DLLs, but only for installation
rem -------------------------------------------------
if /i "%MTARGET%" == "" goto :COMPLETION
if /i not "%MTARGET:~-7%" == "install" goto :COMPLETION
rem if not defined INSTALL_ROOT goto :COMPLETION
where/q windeployqt
if ERRORLEVEL 1 goto :COMPLETION
windeployqt --no-translations --no-system-d3d-compiler "%INSTALL_ROOT%\bin"
if ERRORLEVEL 1 exit/b 1

rem show completion time
rem --------------------
:COMPLETION
set ti=%TIME%
set hr=%ti:~0,2%
set mn=%ti:~3,2%
if "%mn:~0,1%" == "0" set mn=%mn:~1,1%
set sc=%ti:~6,2%
if "%sc:~0,1%" == "0" set sc=%sc:~1,1%
set /a tt_secs=((%hr%*3600)+(%mn%*60)+%sc%)-%st_secs%
echo.
echo Completed in %tt_secs% seconds
exit/b 0

