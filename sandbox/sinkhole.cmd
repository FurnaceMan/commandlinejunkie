@echo off

rem Use this when it's preferable to manually sync a Dropbox directory
rem with the current working directory.

setlocal
echo.

rem The command 'tasklist /fi "imagename eq dropbox.exe"' still returns an
rem errorlevel 0 on failure.  The findstr command will return the correct
rem errorlevel.
tasklist | findstr /r /c:"^Dropbox.exe" >nul
if ERRORLEVEL 1 (
    echo Dropbox does not seem to be running
    goto :SOMETHINGWRONG
)


rem Get the dirname of the current working directory
for %%I in (.) do set parentname=%%~nxI
set "DESTDIR=%USERPROFILE%\dropbox\sinkhole\%COMPUTERNAME%\%parentname%"
mkdir "%DESTDIR%" 2>nul


rem TODO adjust to taste, use the file diff utility of your choosing
set "PATH=%ProgramFiles(x86)%\beyond compare 2;%PATH%"
where /q bc2
if ERRORLEVEL 1 (
    echo Please install Beyond Compare
    goto :SOMETHINGWRONG
)

rem Sometimes Dropbox isn't able to create the new directory quickly enough,
rem so double-check it here and let the user know what to do.
if not exist "%DESTDIR%" (
    echo Dropbox may be slow in creating the destination directory, try again.
    goto :SOMETHINGWRONG
)

bc2 . "%DESTDIR%"

endlocal
goto :EOF


:SOMETHINGWRONG
endlocal
echo.
echo %CMDCMDLINE% | find /i "/c" >nul
if ERRORLEVEL 1 exit /b 1
pause
exit/b 1

