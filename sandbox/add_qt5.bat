@echo off

rem TODO adjust as necessary
pushd "%SystemDrive%\Qt\qt5.8.0\5.8\mingw53_32\bin" 2>nul
rem pushd "%SystemDrive%\Qt\qt5.7.0\5.7\mingw53_32\bin" 2>nul
rem pushd "%SystemDrive%\Qt\qt5.6.0\5.6\mingw49_32\bin" 2>nul
if ERRORLEVEL 1 (
      echo [-] Qt 5 environment
      goto :EOF
      )
rem Don't call qtenv2.bat for Qt5.8.0 since it changes the path
rem using forward slashes, which should not be used in Windows.
rem      call qtenv2.bat
set PATH=C:\Qt\Qt5.8.0\5.8\mingw53_32\bin;C:\Qt\Qt5.8.0\Tools\mingw530_32\bin;%PATH%
popd
echo [+] Qt 5
