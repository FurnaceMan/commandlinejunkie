@echo off

rem Loop through the directories in the HOME/src area and reports on
rem Git working directories.

setlocal ENABLEEXTENSIONS
cls
echo.

where /q git
if ERRORLEVEL 1 (
    echo ***** git utility not installed
    goto :SOMETHINGWRONG
)

set "arg1=%~1"
if "%arg1%" == "" set arg1=.

if not "%arg1%" == "." pushd "%arg1%"
if ERRORLEVEL 1 (
    echo ***** %arg1% not found
    goto :SOMETHINGWRONG
)

rem for %%i in (codeslinger bumpy to_dos) do (
for /f "tokens=* usebackq" %%i in (`dir/b/a:d`) do (
    cd "%%i"
    echo.
    echo.
    echo.
    echo --------------------------------------------------
    echo %%i
rem    if exist .git start "%%i" cmd /t:0b /k "(git remote -v & git status)"
    if exist .git (
        git remote -v
        echo --------------------------------------------------
        git status
        echo --------------------------------------------------
        git branch -a
    ) else (
        echo not a git working directory
    )
    cd ..
    timeout /t 1 >nul
)
if not "%arg1%" == "." popd

:COMPLETION
exit/b 0

:SOMETHINGWRONG
exit/b 1

