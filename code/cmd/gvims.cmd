@echo off
setlocal ENABLEEXTENSIONS ENABLEDELAYEDEXPANSION

where/q gvim
if ERRORLEVEL 1 exit/b 1

if exist "%~n1.ui" (
   choice /c YN /t 20 /D N /M "Start Designer on UI file"
   if !ERRORLEVEL! NEQ 2 start /b designer "%~n1.ui"
   )

if exist "%~n1.cpp" if exist "%~n1.h" start /b gvim -o2 "%~n1.h" "%~n1.cpp" & exit/b

if exist "%~n1.cpp" start /b gvim "%~n1.cpp" & exit/b

if exist "%~n1.c" if exist "%~n1.h" start /b gvim -o2 "%~n1.h" "%~n1.c" & exit/b

if exist "%~n1.c" start /b gvim "%~n1.c" & exit/b

where/q codeslinger
if ERRORLEVEL 1 exit/b 0

codeslinger newclass "%~n1"
if ERRORLEVEL 1 exit/b 1

start /b gvim -o2 "%~n1.h" "%~n1.cpp" & exit/b
