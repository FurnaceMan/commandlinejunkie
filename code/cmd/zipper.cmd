@echo off
cls
echo.
setlocal ENABLEEXTENSIONS ENABLEDELAYEDEXPANSION

set "settingsdir=%LOCALAPPDATA%\furnaceman\commandlinejunkie"

where /q 7z
if ERRORLEVEL 1 (
    echo 7zip was not found, please add it to the path
    goto :SOMETHINGWRONG
)

set dt=%DATE%
set tm=%TIME%

if not exist "%settingsdir%" mkdir "%settingsdir%"
if not exist "%settingsdir%\last-dropbox-check.txt" (
    echo.
    echo No dropbox check file
    echo.
    goto :CHECKDROPBOX
)
for /f "delims=" %%l in ('type "%settingsdir%\last-dropbox-check.txt"') do (
    set /a count+=1
    set "Line!count!=%%l"
)
if not "%dt%" == "%Line1%" (
    echo.
    echo Today is "%dt%"
    echo Last dropbox check was "%Line1%"
    echo.
    goto :CHECKDROPBOX
)

rem this gets time values as numbers
set prev_nhr=%Line2:~0,2%
set prev_nmn=%Line2:~3,2%
if "%prev_nmn:~0,1%" == "0" set prev_nmn=%prev_nmn:~1,1%
set prev_nsc=%Line2:~6,2%
if "%prev_nsc:~0,1%" == "0" set prev_nsc=%prev_nsc:~1,1%
set /a st_secs=(%prev_nhr%*3600)+(%prev_nmn%*60)+%prev_nsc%
rem and calculates the time difference in seconds
set nhr=%tm:~0,2%
set nmn=%tm:~3,2%
if "%nmn:~0,1%" == "0" set nmn=%nmn:~1,1%
set nsc=%tm:~6,2%
if "%nsc:~0,1%" == "0" set nsc=%nsc:~1,1%
set /a diff_secs=((%nhr%*3600)+(%nmn%*60)+%nsc%)-%st_secs%

rem echo It has been %diff_secs% seconds since the last drop box check

rem Skip the check if it has been less than two hours ago
rem since it is likely to still be running.  YMMV
if %diff_secs% LSS 7200 goto :SKIPDROPBOXCHECK


:CHECKDROPBOX
echo Making sure dropbox is running, please wait...
tasklist /fi "username eq %USERNAME%" | findstr /r /c:"^Dropbox.exe" >nul
if ERRORLEVEL 1 (
    echo Dropbox does not seem to be running
    goto :SOMETHINGWRONG
)
> "%settingsdir%\last-dropbox-check.txt" echo %dt%
>> "%settingsdir%\last-dropbox-check.txt" echo %tm%


:SKIPDROPBOXCHECK
set dt=%DATE%
set tm=%TIME%
rem this gets date and time as a string
set yr=%dt:~10,4%
set mo=%dt:~4,2%
set dy=%dt:~7,2%
set hr=%tm:~0,2%
if "%hr:~0,1%" == " " set hr=0%tm:~1,1%
set mn=%tm:~3,2%
rem get the number of seconds remaining in the minute
set nsc=%tm:~6,2%
if "%nsc:~0,1%" == "0" set nsc=%nsc:~1,1%
set /a rem_secs=60-%nsc%

set "STASHDIR=%USERPROFILE%\dropbox\stash\%yr%%mo%%dy%-%hr%%mn%"
if not exist "%STASHDIR%" mkdir "%STASHDIR%"

set "arg1=%~1"

if "%arg1%" == "" set "arg1=."

pushd "%arg1%"
if ERRORLEVEL 1 goto :SOMETHINGWRONG

for /f "tokens=*" %%d in ("%CD%") do set "dirname=%%~nd"

if not exist "%STASHDIR%" (
    echo.
    echo Was unable to create stash directory
    echo %STASHDIR%
    popd
    goto :SOMETHINGWRONG
)

if exist "%STASHDIR%\%dirname%.7z" (
    echo.
    echo %dirname%.7z already exists, please wait %rem_secs% seconds
    timeout /t %rem_secs%
    popd
    goto :SOMETHINGWRONG
    )

rem TODO make sure we aren't already in root directory
cd ..
7z a "%STASHDIR%\%dirname%.7z" "%dirname%"
>> "%STASHDIR%\readme.txt" echo %dirname%.7z : %COMPUTERNAME% : %CD%

popd


:COMPLETION
echo.
echo Success...
echo.
echo %CMDCMDLINE% | find /i "/c" >nul
if ERRORLEVEL 1 exit /b 0
pause
exit /b 0


:SOMETHINGWRONG
echo.
echo Something went wrong...
echo.
echo %CMDCMDLINE% | find /i "/c" >nul
if ERRORLEVEL 1 exit /b 1
pause
exit /b 1

