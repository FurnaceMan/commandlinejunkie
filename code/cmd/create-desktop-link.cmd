@echo off
setlocal
echo.

set arg0=%~dp0
set arg0=%arg0:~0,-1%
if not "%arg0%" == "%CD%" (
      echo Run this script from where it sits
      goto :SOMETHINGWRONG
      )

set "arg1=%~1"
if not exist "%arg1%" (
      echo %arg1% not found
      goto :SOMETHINGWRONG
      )

set "arg2=%~2"
if "%arg2%" == "" set arg2=0b

echo.
echo ------------------------------------------------------------
echo Creating a desktop shortcut
> doit.vbs echo.
>> doit.vbs echo Set oWS = WScript.CreateObject("WScript.Shell")
>> doit.vbs echo sLinkFile = "%HOMEDRIVE%%HOMEPATH%\Desktop\%~n1.lnk"
>> doit.vbs echo Set oLink = oWS.CreateShortcut(sLinkFile)
>> doit.vbs echo oLink.WorkingDirectory = "%CD%"
>> doit.vbs echo oLink.TargetPath = "%ComSpec%"
>> doit.vbs echo oLink.Arguments = "/T:%arg2% /A /K ""%CD%\%~nx1"""
>> doit.vbs echo oLink.Save
cscript doit.vbs
if ERRORLEVEL 1 (
      echo Unable to run the doit.vbs script
      exit/b 1
      )
del doit.vbs

:COMPLETION
exit/b 0

:SOMETHINGWRONG
exit/b 1
