@echo off

rem add, or prepend, a desired directory path to the path
rem
rem see pathconfig.cmd for keyword based custom locations

if not exist "%LOCALAPPDATA%\transient" mkdir "%LOCALAPPDATA%\transient"
set "tmpfile=%LOCALAPPDATA%\transient\setnewpath.bat"

setlocal ENABLEEXTENSIONS ENABLEDELAYEDEXPANSION

if "%~1" == "" goto :USAGE

pushd "%~dp0"
if exist pathconfig-%COMPUTERNAME%.cmd (
    call pathconfig-%COMPUTERNAME%.cmd
) else (
    call pathconfig.cmd
)
popd

set prepend=0
if /i "%~n0" == "prepath" set prepend=1
set "p1=%~1"
set "dirname=%~1"
set "dirlist=%~1"

if /i "%p1%" == "7zip" (
    set "dirname=%ZIPPATH%"
    set "dirlist=%ZIPPATH%"
)
if /i "%p1%" == "axcrypt" (
    set "dirname=%AXCRYPTPATH%"
    set "dirlist=%AXCRYPTPATH%"
)
if /i "%p1%" == "bc2" (
    set "dirname=%BC2PATH%"
    set "dirlist=%BC2PATH%"
)
if /i "%p1%" == "doxygen" (
    set "dirname=%DOXYGENPATH%"
    set "dirlist=%DOXYGENPATH%"
)
if /i "%p1%" == "gitcmd" (
    set "dirname=%GITCMDPATH%"
    set "dirlist=%GITCMDPATH%"
)
if /i "%p1%" == "inno" (
    set "dirname=%INNOPATH%"
    set "dirlist=%INNOPATH%"
)
if /i "%p1%" == "jdk" (
    set "dirname=%JDKPATH%"
    set "dirlist=%JDKPATH%"
)
if /i "%p1%" == "jre" (
    set "dirname=%JREPATH%"
    set "dirlist=%JREPATH%"
)
if /i "%p1%" == "perl" (
    set "dirname=%PERLPATH%"
    set "dirlist=%PERLPATH%"
)
if /i "%p1%" == "psh" (
    set "dirname=%PSHPATH%"
    set "dirlist=%PSHPATH%"
)
if /i "%p1%" == "python" (
    set "dirname=%PYTHONPATH%"
    set "dirlist=%PYTHONPATH%"
)
if /i "%p1%" == "qtming32" (
    set "dirname=%QTMING32PATH%"
    set "dirlist=%QTMING32PATHLIST%"
)
if /i "%p1%" == "qtming64" (
    set "dirname=%QTMING64PATH%"
    set "dirlist=%QTMING64PATHLIST%"
)
if /i "%p1%" == "vim" (
    set "dirname=%VIMPATH%"
    set "dirlist=%VIMPATH%"
)

if exist "%tmpfile%" del "%tmpfile%"

if exist "%dirname%" (
   if %prepend% EQU 1 (
> "%tmpfile%" echo echo [+] ^^^< %~1
>> "%tmpfile%" echo set "PATH=%dirlist%;%%PATH%%"
   ) else (
> "%tmpfile%" echo echo [+] ^^^> %~1
>> "%tmpfile%" echo set "PATH=%%PATH%%;%dirlist%"
   )
) else (
> "%tmpfile%" echo echo [-] %~1
>> "%tmpfile%" echo exit/b 1
)

endlocal

call "%tmpfile%"
set ecode=%ERRORLEVEL%
set tmpfile=
exit/b %ecode%

:USAGE
rem Use addpath/prepath with no arguments to display usage
set tmpfile=
echo.
echo usage: addpath^|prepath [KEYWORD^|DIRECTORY]
echo.
echo Keywords:
echo    7zip       7-zip
echo    axcrypt    AxCrypt
echo    bc2        Beyond Compare 2
echo    doxygen    Doxygen
echo    gitcmd     Git Console Command
echo    inno       InnoSetup
echo    jdk        Java Development Kit
echo    jre        Java Runtime Environment
echo    perl       Perl
echo    psh        Windows Powershell
echo    python     Python
echo    qtming32   Qt 5 MinGW 32-bit Development Framework
echo    qtming64   Qt 5 MinGW 64-bit Development Framework
echo    vim        gVim
exit/b 0

