@echo off
cls
echo.
cd /d "%~dp0"

call setminpath
if exist ..\etc\macrolist.txt doskey /macrofile=..\etc\macrolist.txt

:COMPLETION
echo.
if defined HOME (
    echo HOME = %HOME%
    if not exist "%HOME%" (
        echo !!! HOME is not defined correctly
    ) else (
        cd /d "%HOME%"
    )
) else (
      echo USERPROFILE = %USERPROFILE%
      cd /d "%USERPROFILE%"
)

if defined EDITOR echo EDITOR = %EDITOR%

echo.
prompt $_$P$_$+$G$S

