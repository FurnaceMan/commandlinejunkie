@echo off
setlocal ENABLEDELAYEDEXPANSION ENABLEEXTENSIONS

rem usage:  bumpy [new|show|major|minor|patch]
rem
rem Generates / updates the buildversion.cmd, .h, .iss, .pri, .sh files.
rem
rem Use this script with 'new' to create a new buildversion.cmd file.  The
rem buildversion.cmd file sets the VERSION_MAJOR, VERSION_MINOR and
rem VERSION_PATCH environment variables.
rem
rem Use 'show' to simply display the current version.
rem
rem Use 'major', 'minor' or 'patch' to increment the appropriate digit
rem of the version id in the buildversion.cmd file and then re-create the
rem associated buildversion.* files, depending on whether they exist.

set "HEADERDIR=%CD%"

if /i "%~1" == "new" (
      if exist buildversion.cmd (
         echo error: buildversion.cmd exists, delete it first
         exit/b 1
      )
      set VERSION_MAJOR=0
      set VERSION_MINOR=1
      set VERSION_PATCH=0
> buildversion.cmd echo.
> buildversion.h echo.
> buildversion.iss echo.
> buildversion.pri echo.
> buildversion.sh echo.
      goto :MAKEVERSION
      )

rem not having a buildversion file isn't necessarily an error
if not exist buildversion.cmd exit/b 0

rem The buildversion.h file may be in a private subdirectory
if not exist buildversion.h call :FINDHEADER

call buildversion.cmd

if /i "%~1" == "show" goto :SHOWVERSION

if /i "%~1" == "major" (
      set /a VERSION_MAJOR=%VERSION_MAJOR%+1
      set VERSION_MINOR=0
      set VERSION_PATCH=0
      goto :MAKEVERSION
      )

if /i "%~1" == "minor" (
      set /a VERSION_MINOR=%VERSION_MINOR%+1
      set VERSION_PATCH=0
      goto :MAKEVERSION
      )

if /i "%~1" == "patch" (
      set /a VERSION_PATCH=%VERSION_PATCH%+1
      goto :MAKEVERSION
      )

echo usage:  bumpy new^|show^|major^|minor^|patch
exit/b 1

:MAKEVERSION

> buildversion.cmd (echo rem Adjusted by bumpy on %DATE% %TIME%)
>> buildversion.cmd (echo set VERSION_MAJOR=%VERSION_MAJOR%)
>> buildversion.cmd (echo set VERSION_MINOR=%VERSION_MINOR%)
>> buildversion.cmd (echo set VERSION_PATCH=%VERSION_PATCH%)

if exist "%HEADERDIR%\buildversion.h" (
> "%HEADERDIR%\buildversion.h" (echo #ifndef BuildVersion)
>> "%HEADERDIR%\buildversion.h" (echo #define BuildVersion "%VERSION_MAJOR%.%VERSION_MINOR%.%VERSION_PATCH%")
>> "%HEADERDIR%\buildversion.h" (echo #endif)
)

if exist buildversion.iss (
> buildversion.iss (echo #define BuildVersion "%VERSION_MAJOR%.%VERSION_MINOR%.%VERSION_PATCH%")
)

if exist buildversion.pri (
> buildversion.pri (echo VERSION = %VERSION_MAJOR%.%VERSION_MINOR%.%VERSION_PATCH%)
)

if exist buildversion.sh (
> buildversion.sh (echo export VERSION_MAJOR=%VERSION_MAJOR%)
>> buildversion.sh (echo export VERSION_MINOR=%VERSION_MINOR%)
>> buildversion.sh (echo export VERSION_PATCH=%VERSION_PATCH%)
)


:SHOWVERSION
echo.
echo %VERSION_MAJOR%.%VERSION_MINOR%.%VERSION_PATCH%

exit/b 0

:FINDHEADER
for /f %%I in ('dir/s/b buildversion.h') do (
      echo found %%I
      set HEADERDIR=%%~dpI
      )
goto :EOF

