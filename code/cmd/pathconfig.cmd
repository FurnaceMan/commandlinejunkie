set "ZIPPATH=%ProgramFiles%\7-zip"
set "AXCRYPTPATH=%ProgramFiles%\Axantum\AxCrypt"
set "DOXYGENPATH=%ProgramFiles%\doxygen\bin"
set "BC2PATH=%SystemDrive%\progra~2\beyond compare 2"
set "GITCMDPATH=%SystemDrive%\progra~1\git\cmd"
set "INNOPATH=%ProgramFiles(x86)%\Inno Setup 5"
set "JDKPATH=%ProgramFiles%\Java\jdk1.8.0_201\bin"
set "JREPATH=%ProgramFiles%\Java\jre1.8.0_201\bin"
set "PERLPATH=%SystemDrive%\Perl64\bin"
set "PSHPATH=%SystemRoot%\System32\WindowsPowerShell\v1.0"
rem set "PYTHONPATH=%LOCALAPPDATA%\Programs\Python\Python37-32"
set "VIMPATH=%SystemDrive%\Progra~2\vim\vim82"
set "QTMING32PATH=%SystemDrive%\Qt\Qt5.14.2\5.14.2\mingw73_32\bin"
set "QTMING32PATHLIST=%SystemDrive%\Qt\Qt5.14.2\5.14.2\mingw73_32\bin;%SystemDrive%\Qt\Qt5.14.2\Tools\mingw730_32\bin"
set "QTMING64PATH=%SystemDrive%\Qt\Qt5.14.2\5.14.2\mingw73_64\bin"
set "QTMING64PATHLIST=%SystemDrive%\Qt\Qt5.14.2\5.14.2\mingw73_64\bin;%SystemDrive%\Qt\Qt5.14.2\Tools\mingw730_64\bin"
