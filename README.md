# Command Line Junkie

A collection of helpful Windows command scripts that make using the command
shell less tedious.

Unix shell scripts will be added in the future.

## Requirements

This is built for use on a recent Windows operating system.  This is tested
out on a Windows 11 system.  Scripts may work with older version of Windows.

The 7zip utility is used to bundle up the files into an installation bundle.
Adjust the delivery script with your favorite packaging utility.

