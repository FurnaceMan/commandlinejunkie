@echo off

rem Creates a zip file of all that is necessary.
rem This takes a command line parameter that is used to name the top level
rem directory, or 'wincmd' if none specified.

setlocal ENABLEEXTENSIONS ENABLEDELAYEDEXPANSION
color
echo.

where/q 7z
if ERRORLEVEL 1 (
    echo 7zip not found, please add to the PATH
    goto :SOMETHINGWRONG
)

set "arg1=%~1"
if "%arg1%" == "" set arg1=wincmd

rem Move to the directory where this script is located
cd /d "%~dp0"

if exist "%arg1%.7z" del "%arg1%.7z"
if exist "%arg1%" rmdir /s /q "%arg1%"
xcopy /s /i ..\code "%arg1%"
copy /y ..\code\cmd\addpath.cmd "%arg1%\cmd\prepath.cmd"
7z a "%arg1%.7z" "%arg1%"
rmdir /s /q "%arg1%"


:COMPLETION
rem The following lines exit out immediately, but only
rem if this is being run directly in a command window,
rem as opposed to being double-clicked in file explorer.
echo %CMDCMDLINE% | find /i "/c" >nul
if ERRORLEVEL 1 exit /b 0

rem hang out a bit before exiting
timeout /t 7
exit/b 0



:SOMETHINGWRONG
rem Turn the screen red to indicate a problem
color 0c

echo %CMDCMDLINE% | find /i "/c" >nul
if ERRORLEVEL 1 exit /b 1

rem hang out until the user does something
pause
exit/b 1

