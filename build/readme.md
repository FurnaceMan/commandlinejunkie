# build documentation

Use the make-bundle script to zip up the necessary files.

Add a name to the make-bundle command line to customize the zip filename and
top level directory.

```
make-bundle codejunkie
```

Will create a 'codejunkie.zip' file with a 'codejunkie' top level directory
when the zip is unpacked.

## installation

To install somewhere, change to the directory where you wish the top level
directory to be located and unzip the file there.

Go to the cmd subdirectory and run the make-desktop-links script to create
desktop links to some good command environments.  Create your own command
environments here if you like.

## Example

Assuming your working directory is this one and the `c:\tools` directory is
where you like to install things and you are a code junkie...

```
make-bundle codejunkie
set "ORIGDIR=%CD%"
pushd c:\tools
7z x "%ORIGDIR%\codejunkie.7z"
cd codejunkie\cmd
create-desktop-link customcommandtool.cmd
popd
set ORIGDIR=
```

